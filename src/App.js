import React from 'react';
import './App.css';
import { Button } from 'semantic-ui-react';
import { Card, Icon, Image } from 'semantic-ui-react'

const GITHUB_USER_INFO = "https://api.github.com/users/davegregg";

class App extends React.Component {
  state = {
    user: {},
    active: false
  }

  handleToggle = () => {
    console.log("button was clicked!");
    if (this.state.active === true) {
      this.setState({active: false})
    } else {
    fetch(GITHUB_USER_INFO).then(response => response.json())
    .then(result =>  {
      console.log(result);
      this.setState({user: result, active: true});
    })
    }
  }
  render () {
    const { user , active} = this.state;
  return (
  <React.Fragment>
     <Button onClick={this.handleToggle}>Toggle</Button>
      {active && (
         <Card>
         <Image src= {user.avatar_url} alt="Avatar" />
         <Card.Content>
           <Card.Header>{user.name}</Card.Header>
           <Card.Description>{user.bio}</Card.Description>
         </Card.Content>
         <Card.Content extra>
         <Icon name='user' />
          {user.followers} Followers
         </Card.Content>
       </Card>
     )}
     </React.Fragment>
  );
      }
    }

export default App;
